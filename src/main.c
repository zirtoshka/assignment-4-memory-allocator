#include "test.h"
#include "stdio.h"

int main() {
    printf("Test number 1:\n");
    test1();

    printf("Test number 2:\n");
    test2();

    printf("Test number 3:\n");
    test3();

    printf("Test number 4:\n");
    test4();

    printf("Test number 5:\n");
    test5();

    return 0;
}